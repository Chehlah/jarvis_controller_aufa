#include "jarvis_body_hand/kinematic.hpp"
/*
 * Description : Modular arm inverse kinematic handler framework
 * Author      : Thanabadee Bulunseechart
 */
#include <Eigen/Dense>


namespace jarvis_body_hand {
	using namespace Eigen;

	Kinematic::Kinematic(ros::NodeHandle& nodeHandle, const std::string &move_group, const std::string &eef, Voice* voice)
												: nodeHandle_(nodeHandle),
													movegroup_(move_group),
													eef_(eef),
													voice_(voice),
													average_(0.0),
													nMeasurements_(0)
	{

		readParameters();
		DEBUG=false;

		double test;
		grippers_state = 0;

		if(movegroup_=="" || eef_=="")
		{
			movegroup_="right_arm";
			eef_ = "right_hand_link";
			ROS_INFO("Kinematic: movegroup isn't set up. we choose movegroup 'right_arm' and end-effector 'right_hand_link' by default");
		}

		robot_model_loader = robot_model_loader::RobotModelLoader("robot_description");
		kinematic_model = robot_model_loader.getModel();
		ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());
		kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(kinematic_model));
		kinematic_state->setToDefaultValues();
		joint_model_group = kinematic_model->getJointModelGroup(movegroup_);

		group = moveit::planning_interface::MoveGroupInterfacePtr(new moveit::planning_interface::MoveGroupInterface(movegroup_));

		gripper_group = moveit::planning_interface::MoveGroupInterfacePtr(new moveit::planning_interface::MoveGroupInterface("right_gripper"));

		joint_names = joint_model_group->getJointModelNames();

		group->setNumPlanningAttempts(5.0);
		group->setGoalOrientationTolerance(0.5);
		group->setGoalPositionTolerance(0.03);
		group->allowReplanning(true);

		planning_scene_diff_publisher = nodeHandle_.advertise<moveit_msgs::PlanningScene>("/planning_scene", 1);
		while (planning_scene_diff_publisher.getNumSubscribers() < 1)
		{
		  ros::WallDuration sleep_t(0.5);
		  sleep_t.sleep();
		}

		object_to_display= nodeHandle_.advertise<moveit_msgs::CollisionObject>("/collision_object",10);

		planning_scene_monitor_.reset(new planning_scene_monitor::PlanningSceneMonitor("robot_description"));

		clear_octomap = nodeHandle_.serviceClient<std_srvs::Empty>("/clear_octomap");

		ROS_INFO("Kinematic: Lauched successfull!!");


	}

	Kinematic::~Kinematic()
	{

		delete joint_model_group;

	}

	void Kinematic::Debug(bool Debug)
	{
		DEBUG = Debug;
		if(DEBUG) ROS_INFO("Kinematic: Set Debug mode = True");
	}
	void Kinematic::SetMoveGroup(const std::string &move_group, const std::string &eef) 
	{
		movegroup_ = move_group;
		eef_ = eef;
	}
	double Kinematic::GetGripState() { return grippers_state; }

	bool Kinematic::SetGripperOpen(bool OPEN)
	{
		const double OPEN_GRIPPERS = GRIP_OPEN;
		const double CLOSE_GRIPPERS = GRIP_CLOSE;
		grippers_state = (OPEN ? OPEN_GRIPPERS:CLOSE_GRIPPERS);

		std::vector<double> group_variable_values;
		group_variable_values.push_back(grippers_state);
		group_variable_values.push_back(-grippers_state);
		gripper_group->setJointValueTarget(group_variable_values);
		bool success = gripper_group->plan(my_plan);
		gripper_group->move();

		return success;
	}
	bool Kinematic::GrapAt(Eigen::Vector3d trans, Eigen::Vector3d rot)
	{
		bool success = false;
		SetGripperOpen(true);
		
		for(int i =0;i<8;i++)
		{	
			/* try to move up/down around 4 cm up - 4 cm down */
			success = MoveArmTo(trans+Eigen::Vector3d(0,0,0.01*(float)i-0.04), rot);
			ROS_INFO("Kinetic : try to move %d", i);
			if(success)	break;
		}
		//TODO(MAX): Replace sleep with position distance check
		sleep(2.0);
		if(success) {
			group->attachObject("box", "right_hand_link");
			arm_has_object = true;
		}
		SetGripperOpen(false);
		return success;
	}
	bool Kinematic::PlaceAt(Eigen::Vector3d trans, Eigen::Vector3d rot)
	{
		bool success = false;
		SetGripperOpen(false);
		for(int i =0;i<8;i++)
		{	
			/* try to move up/down around 4 cm up - 4 cm down */
			success = MoveArmTo(trans+Eigen::Vector3d(0,0,0.01*(float)i-0.04), rot);
			ROS_INFO("Kinetic : try to move %d", i);
			if(success)	break;
		}
		if(success) {
			group->detachObject("box");
			arm_has_object = false;
			RemoveBox("box");
			//TODO(MAX): Replace sleep with position distance check
			sleep(4.0);
			SetGripperOpen(true);
		}
		
		return success;
	}

	bool Kinematic::getPos(std::vector<double> &trans,std::vector<double>&rot)
	{
		
	}
/*
put setJointAngleModel
get kinematic_state updated with current joint act

Input goto (x,y,z,ox,oy,oz,ow)
backend :
	Check position is valid? if not return false
	Check 


*/

	void Kinematic::setJointAngleModel(const control_msgs::FollowJointTrajectoryFeedback::ConstPtr& joint_states_)
	{
	//Usage based on model
		if(DEBUG)
			ROS_INFO("-------SETTING JOINT -------");

		joint_values.clear();
		for(int i=0;i<JOINT_N;i++)
			joint_values.push_back(joint_states_->actual.positions[i]);

	//Set current joint position
		kinematic_state->setJointGroupPositions(joint_model_group, joint_values);

		if(DEBUG)
			ROS_INFO("-------SET JOINT DONE-------");
	}

	void Kinematic::setJointAngleModel(const sensor_msgs::JointState::ConstPtr &joint_states_)
	{
	//Usage based on RVIZ
		joint_values.clear();
		for(int i=0;i<JOINT_N;i++)
			joint_values.push_back(joint_states_->position[ (int)JOINT_RVIZ[i] ]);

	//Set current joint position
		kinematic_state->setJointGroupPositions(joint_model_group, joint_values);
	}



	void Kinematic::CurrentPose(Eigen::Affine3d &end_effector_state)
	{	if(DEBUG)
		ROS_INFO("-------CHECK WHERE MODEL EEF -------");

		end_effector_state = kinematic_state->getGlobalLinkTransform(eef_);

		if(DEBUG)
		{
			ROS_INFO_STREAM("Translation: " << end_effector_state.translation());
			ROS_INFO_STREAM("Rotation: " << end_effector_state.rotation());
			ROS_INFO("-------CHECK WHERE MODEL EEF DONE -------");
		}

	}
	bool Kinematic::pToJ(Eigen::Vector3d trans ,Eigen::Vector3d rot)
	{
		if(DEBUG)
			ROS_INFO("-------SET EEF TO POSITION BY MODEL -------");
	//Inverse Kinematic

		Eigen::AngleAxisd rollAngle(rot[0], Eigen::Vector3d::UnitX());
		Eigen::AngleAxisd pitchAngle(rot[1], Eigen::Vector3d::UnitY());
		Eigen::AngleAxisd yawAngle(rot[2], Eigen::Vector3d::UnitZ());

		Eigen::Quaternion<double> q = yawAngle * pitchAngle * rollAngle;

		geometry_msgs::Pose Dp;
		Dp.position.x = trans[0];
		Dp.position.y = trans[1];
		Dp.position.z = trans[2];
		Dp.orientation.x = q.x();
		Dp.orientation.y = q.y();
		Dp.orientation.z = q.z();
		Dp.orientation.w = q.w();

		ROS_INFO_STREAM("TARGET : " << Dp);


		bool found_ik = kinematic_state->setFromIK(joint_model_group, Dp, 10, 0.1);

	//Now, we can print out the IK solution (if found):
		if (found_ik)
		{
			kinematic_state->copyJointGroupPositions(joint_model_group, joint_targets);
		}
		else
		{
			ROS_INFO("Did not find IK solution");
			return false;
		}

		if(DEBUG) 
		{

		//Joint limit
		/* Check whether any joint is outside its joint limits */
			ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));

		/* Enforce the joint limits for this state and check again*/
			kinematic_state->enforceBounds();
			ROS_INFO_STREAM("Current state is " << (kinematic_state->satisfiesBounds() ? "valid" : "not valid"));
		}



	 // Print end-effector pose. Remember that this is in the model frame 
		Eigen::Affine3d end_effector_state;
		CurrentPose(end_effector_state);

		if(DEBUG) 
		{
			ROS_INFO_STREAM("Translation: " << end_effector_state.translation());
			ROS_INFO_STREAM("Rotation: " << end_effector_state.rotation());

			ROS_INFO_STREAM(			  joint_targets[0]    << ","
				<< joint_targets[1]     << ","
				<< joint_targets[2]    << ","
				<< joint_targets[3]  << ","
				<< joint_targets[4]     << ",0,0,0");

			ROS_INFO("-------SET EEF TO POSITION BY MODEL DONE-------");
		}


		return found_ik;
	}

	bool Kinematic::MoveArmTo(Eigen::Vector3d trans, Eigen::Vector3d rot)
	{

		return MoveArmTo(trans, rot, false);
	}

	//Please replace with argument (replace some joint [arrayofJointId, arrayofJointValue to replace])
	bool Kinematic::MoveArmTo(Eigen::Vector3d trans, Eigen::Vector3d rot, bool wrist_up)
	{

		Eigen::Affine3d end_effector_state;
		CurrentPose(end_effector_state);



		if( pToJ(trans, rot) )
		{

			
	  // ros::Publisher display_publisher = nodeHandle_.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
	  // moveit_msgs::DisplayTrajectory display_trajectory;

			if(DEBUG) 
			{
				ROS_INFO("Reference frame: %s", group->getPlanningFrame().c_str());
				ROS_INFO("Reference frame: %s", group->getEndEffectorLink().c_str());
			}


			//Planning to a joint-space goal
			// Let’s set a joint space goal and move towards it. This will replace the pose target we set above.
			// First get the current set of joint values for the group->
			std::vector<double> group_variable_values;
			group->getCurrentState()->copyJointGroupPositions(group->getCurrentState()->getRobotModel()->getJointModelGroup(group->getName()), group_variable_values);


	  		//Now, let’s modify one of the joints, plan to the new joint space goal and visualize the plan.
			group_variable_values = joint_targets;
			if(wrist_up) group_variable_values[3] = -1.2;
			group->setJointValueTarget(group_variable_values);
			// group->setPositionTarget(trans(0), trans(1), trans(2), eef_);
			bool success = group->plan(my_plan);
			group->move();

			if(DEBUG) 
			{
				ROS_INFO("Visualizing plan 2 (joint space goal) %s",success?"":"FAILED");
	  	/* Sleep to give Rviz time to visualize the plan. */
				ROS_INFO("-------SET ALL LINK TO SAME MODEL DONE-------");
	  	// sleep(5.0);
			}
			return success;
		}else
		{
			ROS_ERROR("IK not founded");
			return false;
		}
	}

	bool Kinematic::setHome()
	{
		group->setNamedTarget("right_arm_home");
		bool success = group->plan(my_plan);
		group->move();
		return success;
	}


	void Kinematic::AddBox(Eigen::Vector3d trans, Eigen::Vector3d rot, Eigen::Vector3d size, std::string NameID)
	{

		Eigen::AngleAxisd rollAngle(rot[0], Eigen::Vector3d::UnitX());
		Eigen::AngleAxisd pitchAngle(rot[1], Eigen::Vector3d::UnitY());
		Eigen::AngleAxisd yawAngle(rot[2], Eigen::Vector3d::UnitZ());

		Eigen::Quaternion<double> q = yawAngle * pitchAngle * rollAngle;


		moveit_msgs::CollisionObject collision_object;
		collision_object.header.frame_id = group->getPlanningFrame();

		/* The id of the object is used to identify it. */
		collision_object.id = NameID;

		/* Define a box to add to the world. */
		shape_msgs::SolidPrimitive primitive;
		primitive.type = primitive.BOX;
		primitive.dimensions.resize(3);
		primitive.dimensions[0] = size(0);
		primitive.dimensions[1] = size(1);
		primitive.dimensions[2] = size(2);

		/* A pose for the box (specified relative to frame_id) */
		geometry_msgs::Pose box_pose;
		box_pose.orientation.w = q.w();
		box_pose.orientation.x = q.x();
		box_pose.orientation.y = q.y();
		box_pose.orientation.z = q.z();
		box_pose.position.x =  trans(0);
		box_pose.position.y =  trans(1);
		box_pose.position.z =  trans(2);

		collision_object.primitives.push_back(primitive);
		collision_object.primitive_poses.push_back(box_pose);
		collision_object.operation = collision_object.ADD;

		std::vector<moveit_msgs::CollisionObject> collision_objects;
		collision_objects.push_back(collision_object);


		ROS_INFO("Kinetic: Add an object into the world");
		planning_scene_interface.addCollisionObjects(collision_objects);

		/* Sleep so we have time to see the object in RViz */
		sleep(2.0);

		group->setPlanningTime(10.0);



	}

	void Kinematic::RemoveBox(std::string NameID)
	{
		moveit_msgs::CollisionObject collision_object;
		collision_object.header.frame_id = group->getPlanningFrame();

		/* The id of the object is used to identify it. */
		collision_object.id = NameID;
		collision_object.operation = collision_object.REMOVE;

		std::vector<moveit_msgs::CollisionObject> collision_objects;
		collision_objects.push_back(collision_object);


		ROS_INFO("Kinetic: Removing an object into the world");
		planning_scene_interface.addCollisionObjects(collision_objects);
	}

	void Kinematic::ResetOctomap()
	{
		std_srvs::Empty srv;
		clear_octomap.call(srv);
	}

	void Kinematic::AddObj(Eigen::Vector3d trans, Eigen::Vector3d rot, Eigen::Vector3d size, std::string NameID)
	{
		ResetOctomap();
		
		group->detachObject(NameID);

		Eigen::AngleAxisd rollAngle(rot[0], Eigen::Vector3d::UnitX());
		Eigen::AngleAxisd pitchAngle(rot[1], Eigen::Vector3d::UnitY());
		Eigen::AngleAxisd yawAngle(rot[2], Eigen::Vector3d::UnitZ());

		Eigen::Quaternion<double> q = yawAngle * pitchAngle * rollAngle;


		
		attached_object.link_name = "right_hand_link";
		/* The header must contain a valid TF frame*/
		// attached_object.object.header.frame_id  = group->getPlanningFrame();
		/* The id of the object */
		attached_object.object.id = NameID;

		/* A default pose */
		geometry_msgs::Pose pose;
		pose.orientation.w = q.w();
		pose.orientation.x = q.x();
		pose.orientation.y = q.y();
		pose.orientation.z = q.z();
		pose.position.x =  trans(0);
		pose.position.y =  trans(1);
		pose.position.z =  trans(2);

		/* Define a box to be attached */
		shape_msgs::SolidPrimitive primitive;
		primitive.type = primitive.BOX;
		primitive.dimensions.resize(3);
		primitive.dimensions[0] = size(0);
		primitive.dimensions[1] = size(1);
		primitive.dimensions[2] = size(2);

		attached_object.object.primitives.clear(); //Added
		attached_object.object.primitives.push_back(primitive);
		attached_object.object.primitive_poses.push_back(pose);

		attached_object.object.operation = attached_object.object.ADD;
		ROS_INFO("Kinetic : Adding the object into the world ..");
		moveit_msgs::PlanningScene planning_scene;
		planning_scene.world.collision_objects.clear();
		planning_scene.world.collision_objects.push_back(attached_object.object);
		planning_scene.is_diff = true;
		planning_scene_diff_publisher.publish(planning_scene);

		/* Sleep so we have time to see the object in RViz */
		sleep(2.0);

		group->setPlanningTime(10.0);



	}

	void Kinematic::getInverseK(double In[3],double Out[5]) const
	{
		double x,
		y,
		z,
		k1,
		k2,
		a1,
		a2,
		sin_th3,
		cos_th3,
		th1,
		th2,
		th3,
		th4,
		cos_phi,
		sin_phi;

	a1 = 1; //get param
	a2 = 1; //get param
	cos_th3 = (x*x + z*z - a1*a1 - a2*a2)/(x*a1*a2);
	sin_th3 = /*+-*/ sqrt(1 - cos_th3*cos_th3);
	


	k1 = a1 + a2*cos_th3;
	k2 = a2*sin_th3;

	cos_phi = cos(th2+th3+th4); //where we get th4???
	sin_phi = sin(th2+th3+th4);

	th1 = atan(y/z);
	th2 = atan(z/x) - atan(k2/k1);
	th3 = atan(sin_th3/cos_th3);
	th4 = atan(sin_phi/cos_phi) - th2 - th3;

}

void Kinematic::getForwardK(double In[5],double Out[3]) const
{
}




bool Kinematic::readParameters()
{
	if(!nodeHandle_.getParam("/JOINT_N", JOINT_N) ) 
		return false;
	nodeHandle_.getParam("/JOINT_RVIZ", JOINT_RVIZ);
	nodeHandle_.getParam("/GRIP_CLOSE", GRIP_CLOSE);
	nodeHandle_.getParam("/GRIP_OPEN", GRIP_OPEN);

	return true;
}



} /* namespace */
