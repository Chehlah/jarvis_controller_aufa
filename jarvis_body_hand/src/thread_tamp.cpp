#include "jarvis_body_hand/actuator.hpp"



namespace jarvis_body_hand {

Actuator::Actuator(ros::NodeHandle& nodeHandle)
    : nodeHandle_(nodeHandle)
{

	STOP_THREAD = false;
	REQUEST_RESET = true;
	PAUSED=false;

	readParameters();

	//INITIALIZE VARIABLE
	for(uint8_t j=0; j<JOINT_GROUP.size(); j++)
	    for(uint8_t i =0; i < JOINT_N; i++)
	      pubMot.push_back(nodeHandle_.advertise<std_msgs::Float64>
	      	("/arm" + std::to_string(JOINT_START_IDX[j]+i) + "_controller/command", 10));

}

Actuator::~Actuator()
{
}






void Actuator::Run() 
{
	while(ros::ok())
	{
		//TODO check empty file , etc
		if( !STOP_THREAD ) 
		{
			//DO SOMETHING
			ROS_INFO("ACTUATOR_RUNNING_BITCH!")
			//Safe area to stop
			if(STOP_THREAD)
			{
				// break;
				while(STOP_THREAD && ros::ok()) {
					PAUSED = true;
					sleep(0.5);
				}
			}
			PAUSED=false;
		}else
		{
			PAUSED = true;
		}
		sleep(0.1);
	}
}

bool Actuator::Stop() 
{
	STOP_THREAD=true;
	return PAUSED;
}
void Actuator::RequestReset() {
	REQUEST_RESET = true;
	STOP_THREAD = false;
	
}
bool Actuator::FINISH() const
{
	return MotorListMsgQueue.size()==0;
}

void Actuator::readParameters()
{
	nodeHandle_.getParam("/JOINT_N", JOINT_N);
	nodeHandle_.getParam("/JOINT_SCALE", JOINT_SCALE);
	nodeHandle_.getParam("/JOINT_RAW_HOME", JOINT_RAW_HOME);
	nodeHandle_.getParam("/JOINT_START_IDX", JOINT_START_IDX);
	nodeHandle_.getParam("/JOINT_GROUP", JOINT_GROUP);
	if(JOINT_RAW_HOME.size() != JOINT_SCALE.size()) 
	{
		ROS_ERROR("RECORD_HAND: Could not read parameters. Check size of parameters. Please use roslaunch to include param.");
    	ros::requestShutdown();
		while(ros::ok());
	}
}


} /* namespace */
