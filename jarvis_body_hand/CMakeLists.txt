cmake_minimum_required(VERSION 2.8.3)
project(jarvis_body_hand)

## Use C++11
add_definitions(--std=c++11)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake-modules/")


##STILL NOT WORK WITH HARK, CAN'T FIND HARK_MSGS
# set(USE_SOUND_LOCALIZATION TRUE)
# add_definitions(-DUSE_SOUND_LOCALIZATION=${USE_SOUND_LOCALIZATION})

# if (USE_SOUND_LOCALIZATION)
# endif() 
############################################



## Find catkin macros and libraries
find_package(catkin REQUIRED
  COMPONENTS
    roscpp
    sensor_msgs
    dynamixel_msgs
    rosbag
    moveit_core
    moveit_ros_planning
    moveit_ros_planning_interface
    tf_conversions




  cv_bridge
  image_transport
  sensor_msgs
  shape_msgs
  visualization_msgs
  pcl_ros

)




find_package(OpenCV)
include_directories(${OpenCV_INCLUDE_DIRS})


find_package(eigen3 QUIET)
if (NOT EIGEN3_FOUND)
     find_package(PkgConfig REQUIRED)
     #Eigen3 is required, so REQUIRE the second attempt
     pkg_check_modules(EIGEN3 REQUIRED eigen3)
endif()
###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS
    include
#  LIBRARIES
  CATKIN_DEPENDS
    sensor_msgs
    dynamixel_msgs
    control_msgs
    trajectory_msgs
    rosbag
    moveit_core
    moveit_ros_planning_interface
    interactive_markers
    tf_conversions
    

    pcl_ros

    
#  DEPENDS
)





###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a cpp library
add_library(${PROJECT_NAME}_core
   src/Algorithm.cpp
   src/record_hand_position.cpp
   src/kinematic.cpp
   src/actuator.cpp
   src/control_input.cpp
   src/drivebase.cpp
   src/voice.cpp
   # src/perception.cpp
)

## Declare cpp executables
add_executable(${PROJECT_NAME}
  src/${PROJECT_NAME}_node.cpp
  src/System.cpp


)

## Specify libraries to link executable targets against
target_link_libraries(${PROJECT_NAME}_core
  ${catkin_LIBRARIES}
)

target_link_libraries(${PROJECT_NAME}
  ${PROJECT_NAME}_core
  ${catkin_LIBRARIES}
)

#add_executable (drive_base src/drive_base.cpp)
# target_link_libraries (drive_base    ${catkin_LIBRARIES})

add_executable (test_odometry src/test_odometry.cpp)
target_link_libraries (test_odometry    ${catkin_LIBRARIES})


#############
## Install ##
#############

# Mark executables and/or libraries for installation
install(
  TARGETS ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

# Mark cpp header files for installation
install(
  DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.hpp"
)

# Mark other files for installation
install(
  DIRECTORY doc
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

#############
## Testing ##
#############

if(CATKIN_ENABLE_TESTING)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
## Add gtest based cpp test target and link libraries
catkin_add_gtest(${PROJECT_NAME}-test
  test/test_ros_package_template.cpp
  test/AlgorithmTest.cpp)
endif()

if(TARGET ${PROJECT_NAME}-test)
  target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME}_core)
endif()







##########
## TEST ##
##########
find_package(PCL REQUIRED)

include_directories(${PCL_INCLUDE_DIRS})
link_directories(${PCL_LIBRARY_DIRS})
add_definitions(${PCL_DEFINITIONS})

add_executable (region_growing_rgb_segmentation src/pcl_test/region_growing_rgb_segmentation.cpp)
target_link_libraries (region_growing_rgb_segmentation ${PCL_LIBRARIES})

add_executable (planar_segmentation src/pcl_test/planar_segmentation.cpp)
target_link_libraries (planar_segmentation ${PCL_LIBRARIES})

# add_executable (kinect_pcl src/pcl_test/kinect_pcl.cpp)
# target_link_libraries (kinect_pcl ${PCL_LIBRARIES} ${catkin_LIBRARIES})

add_executable (kinect_pcl_without_ros src/pcl_test/kinect_pcl_without_ros.cpp)
target_link_libraries (kinect_pcl_without_ros ${PCL_LIBRARIES})

#add_executable (correspond_test src/pcl_test/correspond_test.cpp)
#target_link_libraries (correspond_test ${PCL_LIBRARIES})

add_executable (test111 src/pcl_test/test.cpp)
target_link_libraries (test111 ${PCL_LIBRARIES})



# add_executable (perception src/perception.cpp)
# target_link_libraries (perception   ${PCL_LIBRARIES} 
#                                     ${catkin_LIBRARIES} 
#                                     ${OpenCV_LIBRARIES})

 



# find_package(HDF5 REQUIRED)
# if(HDF5_FOUND)

#  find_package(FLANN REQUIRED)
#   include_directories(${FLANN_INCLUDE_DIRS})

#  include_directories(${HDF5_INCLUDE_DIR})

#  add_executable(build_tree src/pcl_test/build_tree.cpp)
#  target_link_libraries(build_tree ${PCL_LIBRARIES} ${Boost_LIBRARIES}
#                                 ${FLANN_LIBRARIES} ${HDF5_hdf5_LIBRARY})

#   add_executable(nearest_neighbors src/pcl_test/nearest_neighbors.cpp)
#   target_link_libraries(nearest_neighbors ${PCL_LIBRARIES} 
#                                        ${Boost_LIBRARIES} ${FLANN_LIBRARIES} ${HDF5_hdf5_LIBRARY} 
#                                        )
# endif(HDF5_FOUND)
