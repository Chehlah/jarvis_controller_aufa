#pragma once
#include <ros/ros.h>
#include <std_msgs/String.h>

namespace jarvis_body_hand {

/*!
 * Class containing the algorithmic part of the package.
 */
class Voice
{
 public:
  /*!
   * Constructor.
   */
  Voice(ros::NodeHandle& nodeHandle);

  /*!
   * Destructor.
   */
  virtual ~Voice();

  void Talk(std::string talkStr);



 private:
  ros::NodeHandle& nodeHandle_;
  ros::Publisher pubTTS;
};

} /* namespace */
