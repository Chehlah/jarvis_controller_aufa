#pragma once

/*
 * Description : Do(and read) index using flann & surf
 * Author      : Chanon Onman
 * Editor      : Thanabadee Bulunseechart
 */

#include <cv.h>
#include <highgui.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <iostream>
#include <vector>

//ROS INCLUDE
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <std_msgs/String.h>
#include <tf/transform_listener.h>
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>
// PCL specific includes
//#include <pcl/ros/conversions.h>
#include <pcl/conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>


#include <pcl/ModelCoefficients.h>
#include <pcl/point_types.h>
// #include <pcl/io/pcd_io.h>
// #include <pcl/filters/extract_indices.h>
// #include <pcl/filters/voxel_grid.h>
// #include <pcl/features/normal_3d.h>
// #include <pcl/kdtree/kdtree.h>
// #include <pcl/sample_consensus/method_types.h>
// #include <pcl/sample_consensus/model_types.h>
// #include <pcl/segmentation/sac_segmentation.h>
// #include <pcl/segmentation/extract_clusters.h>
// #include <pcl/filters/passthrough.h>
// #include <pcl/features/vfh.h>

// #include <pcl/kdtree/kdtree_flann.h>

// #include <pcl/features/normal_3d_omp.h>
// #include <pcl/visualization/cloud_viewer.h>







namespace jarvis_body_hand {
using namespace std;
using namespace cv;
/*!
 * Class containing the algorithmic part of the package.
 */
class Perception
{
 public:
  /*!
   * Constructor.
   */
  Perception(ros::NodeHandle& nodeHandle,
             std::string rgb_frame,
             std::string robot_frame);


  void Run();
  bool Stop();
  void RequestReset();
  bool FINISH() const;



  /*!
   * Destructor.
   */
  virtual ~Perception();


 private:

    bool STOP_THREAD;
    bool PAUSED;
    bool REQUEST_RESET;
    void readParameters();

   


    ros::NodeHandle& nodeHandle_;
    ros::Subscriber sub ;
    ros::Subscriber subDepth ;

    ros::Time timeStamp;
    std::string rgb_frame_;// = "/camera_rgb_optical_frame";
    std::string robot_frame_;// = "/camera_link";
    tf::TransformListener* listener;
    ros::Publisher vector_pub; // = n2.advertise<geometry_msgs::Vector3>("object_point", 1000);
    ros::Publisher vector_pub_pour; 
    ros::Publisher vector_pub_pointcloud;
    IplImage *inFrame;//  = cvCreateImage(cvSize(640, 480), 8, 3);
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud_pcl;// (new pcl::PointCloud<pcl::PointXYZRGB>);

    void kinectCallBack(const sensor_msgs::ImageConstPtr& msg);
    void depthCb(const sensor_msgs::PointCloud2& cloud);
    static void mouseCallback(int event, int x, int y, int flags, void *param);
    void doMouseCallback(int event, int x, int y, int flags);

    



};

} /* namespace */
